﻿vars.rnd = dynamic_rnd(32, '#nabcdef');
vars.rnd2 = dynamic_rnd(3, '#n');
vars.rnd3 = dynamic_rnd(4, '#C#C#C#c');

function dynamic_rnd(n, a) {
	a = a.replace('#n', '0987654321');
	a = a.replace('#c', 'zxcvbnmlkjhgfdsaqwertyuiop');
	a = a.replace('#C', 'ZXCVBNMLKJHGFDSAQWERTYUIOP');
	a = a.replace('#r', 'йцукенгшщзхъэждлорпавыфячсмитьбю');
	a = a.replace('#R', 'ЙЦУКЕНГШЩЗХЪЭЖДЛОРПАВЫФЯЧСМИТЬБЮ');
	a = a.replace('#s', ';');
	a = a.replace('#d', ':');
	a = a.replace('#p', ')');
	a = a.replace('#!', '~`@#$%^&*();"\':/?<>.,[]{}|\-_+!=');
	var s = '';
	while (s.length < n) s += a[random(a.length)];
	return s;
}