﻿var serviceKey = 'ключ_от_rucaptcha.com';
var serviceAddress = 'rucaptcha.com';

try {
	// Получение окна в основной вкладке
	var window = getWindow(0);
	// Проверка адреса: адрес с капчей содержит слово 'showcaptcha'
	if(window.location.href.indexOf('/showcaptcha?') != -1) {
		// Поиск картинки с капчей
		var img = null;
		var images = window.document.getElementsByTagName('img');
		for (var i = images.length - 1; i >= 0; i--) {
			if((images[i].src || '').indexOf('captcha') != -1) {
				img = images[i];
				break;
			}
		}
		images = undefined;
		if(img) {
			// Распознавание капчи
			var text = recognize(img, { service: serviceAddress, key: serviceKey, cyrillic: true });
			if (!text) {
				throw new Error('ERROR_CAPTCHA_TIMEOUTED');	 // истекло время распознавания
			}
			// Ввод распознанного текста
			setText('input', 'name=rep', text);
			wait(100);
			submit();
			wait(5000);
		}

		// Получение окна в основной вкладке
		window = getWindow(0);
		// Проверка адреса: адрес с капчей содержит слово 'showcaptcha'
		if(window.location.href.indexOf('/showcaptcha?') != -1) {
			throw new Error('ERROR_CAPTCHA_WRONG_TEXT!'); // не правильно распознана
		}
	}
} catch(e) {
	// вероятно, что-то не так с капчей...
}